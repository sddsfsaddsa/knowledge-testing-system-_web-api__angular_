﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestUseringSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestUsersController : ControllerBase
    {
        private readonly ITestUserService _testUserService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TestUsersController(ITestUserService testUserService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _testUserService = testUserService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetTestUsers()
        {
            var testUsers = _testUserService.GetAllTestUsers();
            return Ok(testUsers);
        }

        [HttpGet("{id}")]
        public IActionResult GetAllTestsOfUser(Guid id)
        {
            var testUsers = _testUserService.GetAllTestUsersWithDetails();
            var testsOfUser = new List<Test>();
            foreach(var testUser in testUsers)
            {
                if(testUser.UserId == id.ToString())
                {
                    testsOfUser.Add(testUser.Test); 
                }
            }
            return Ok(testsOfUser);

        }

        //[HttpGet("{id}", Name = "TestUserById")]
        //public IActionResult GetTestUserWithDetails(int id)
        //{
        //    var testUser = _testUserService.GetTestUserWithDetails(id);
        //    if (testUser == null)
        //    {
        //        return BadRequest("TestUser was not found");
        //    }
        //    return Ok(testUser);
        //}

        [HttpPost]
        public IActionResult CreateTestUser([FromBody] TestUserForCreationDTO testUserDto)
        {
            if (testUserDto == null)
            {
                return BadRequest("TestUser is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            _testUserService.AddTestUser(testUserDto);
            _unitOfWork.Save();

            return Created("/testusers", testUserDto);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateTestUser(int id, [FromBody] TestUserForCreationDTO testUserDto)
        {
            if (testUserDto == null)
            {
                return BadRequest("TestUser is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var testUser = _mapper.Map<TestUser>(_testUserService.GetTestUserById(id));
            if (testUser == null)
            {
                return NotFound($"TestUser with id: {id} hasn't been found");
            }

            _mapper.Map(testUserDto, testUser);

            _testUserService.UpdateTestUser(_mapper.Map<TestUserDTO>(testUser));
            _unitOfWork.Save();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTestUser(int id)
        {
            var testUser = _testUserService.GetTestUserById(id);
            if (testUser == null)
            {
                return BadRequest($"TestUser with id: {id} was not found");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest($"Invalid model object");
            }

            _testUserService.DeleteTestUser(testUser);
            _unitOfWork.Save();

            return NoContent();
        }
    }
}
