import { resolveSanitizationFn } from '@angular/compiler/src/render3/view/template';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../shared/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router : Router, private service : UserService){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(localStorage.getItem('token') !== null){
        let roles = next.data['permittedRoles'] as Array<string>
        if(roles){
          if(this.service.roleMatch(roles)){
            return true
          }
          else{
            this.router.navigate(['forbidden'])
            return false
          }
        }
        return true
      }
      else{
        this.router.navigate(['user/login'])
        return false
      }
  }
  
}
