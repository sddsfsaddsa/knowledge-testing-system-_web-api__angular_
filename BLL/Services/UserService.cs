﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using BLL.Mappers;
using DAL.Contracts;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void AddUser(UserForCreationDTO userDto)
        {
            var u = _mapper.Map<User>(userDto);
            _unitOfWork.User.CreateUser(_mapper.Map<User>(userDto));
        }

        public void DeleteUser(UserDTO userDto)
        {
            _unitOfWork.User.DeleteUser(_mapper.Map<User>(userDto));
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _unitOfWork.User.GetAllUsers()
                .Select(user => _mapper.Map<UserDTO>(user));
        }

        public UserDTO GetUserById(Guid id)
        {
            var u = _unitOfWork.User.GetUserById(id);
            return _mapper.Map<UserDTO>(
                _unitOfWork.User.GetUserById(id)
                );
        }

        public UserDTO GetUserWithDetails(Guid id)
        {
            var user = _unitOfWork.User
                .GetUserWithDetails(id);
            return _mapper.Map<UserDTO>(user);
        }

        public void UpdateUser(UserDTO user)
        {
            _unitOfWork.User.UpdateUser(_mapper.Map<User>(user));
        }
    }
}
