﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Contracts
{
    public interface IQuestionService
    {
        IEnumerable<QuestionDTO> GetAllQuestions();
        QuestionDTO GetQuestionById(int id);
        QuestionDTO GetQuestionWithDetails(int id);
        void AddQuestion(QuestionForCreationDTO questionDto);
        void UpdateQuestion(QuestionDTO questionDto);
        void DeleteQuestion(QuestionDTO questionDto);
    }
}
