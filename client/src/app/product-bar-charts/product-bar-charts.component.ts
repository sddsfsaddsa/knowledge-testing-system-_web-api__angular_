import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-bar-charts',
  templateUrl: './product-bar-charts.component.html',
  styleUrls: ['./product-bar-charts.component.scss']
})
export class ProductBarChartsComponent implements OnInit {

  constructor() { }




  ngOnInit(): void {
  }

  onSelect(event: any) {
    console.log(event);
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  formatString(input: string): string {
    return input.toUpperCase()
  }

  formatNumber(input: number): number {
    return input
  }

}
