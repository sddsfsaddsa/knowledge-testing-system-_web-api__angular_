﻿using DAL.Contracts;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class TestRepository : RepositoryBase<Test>, ITestRepository
    {
        public TestRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public void CreateTest(Test test)
        {
            Create(test);
        }

        public void DeleteTest(Test test)
        {
            Delete(test);
        }

        public IEnumerable<Test> GetAllTests()
        {
            return FindAll().ToList();
        }

        public Test GetTestById(int id)
        {
            return FindByCondition(t => t.Id == id)
                .FirstOrDefault();
        }

        public Test GetTestWithDetails(int id)
        {
            return FindByCondition(t => t.Id == id)
                .Include(t => t.Questions)
                    .ThenInclude(q => q.Answers)
                    .AsSplitQuery()
                .FirstOrDefault();
        }

        public void UpdateTest(Test test)
        {
            Update(test);
        }
    }
}
