﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestResultsController : ControllerBase
    {
        private IMapper _mapper;
        private IUnitOfWork _unitOfwork;
        private ITestResultService _testResultService;
        private ITestService _testService;
        public TestResultsController(IMapper mapper, IUnitOfWork unitOfWork, ITestResultService testResultService, ITestService testService)
        {
            _mapper = mapper;
            _unitOfwork = unitOfWork;
            _testResultService = testResultService;
            _testService = testService;
        }


        [HttpPost]
        public IActionResult AddTestResult([FromBody] TestResultForCreationDTO testResultForCreationDTO)
        {
            if (testResultForCreationDTO == null)
            {
                return BadRequest("Object was null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            _testResultService.AddTestResult(testResultForCreationDTO);
            _unitOfwork.Save();

            return Created("/testresults", _mapper.Map<TestResult>(testResultForCreationDTO));
        }

        [HttpPost("Result")]
        public IActionResult GetResultByUserAndTestId([FromBody] TestResultDTO testResultDTO)
        {
            if (testResultDTO == null)
            {
                return BadRequest("Object was null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var testRes = _testResultService.GetTestResultByUserAndTestId(testResultDTO);
            if (testRes == null)
            {
                return Ok(new
                {
                    passed = false,
                    result = 0
                });
            }

            return Ok(new
            {
                passed = true,
                result = testRes.Result
            });

        }

        [HttpGet("{id}")]
        public IActionResult GetTestStats(int id)
        {
            var test = _testService.GetTestById(id);
            if(test == null)
            {
                return BadRequest($"No test with id: {id}");
            }
            if(!ModelState.IsValid){
                return BadRequest("Invalid model object");
            }

            var testResults = _testResultService.GetAllTestResults().Where(t => t.TestId == id);

            var userIds = new List<string>();
            var data = new List<double>();
            double avgRes = 0;

            foreach(var tr in testResults)
            {
                avgRes += tr.Result;
                userIds.Add(tr.UserId);
                data.Add(tr.Result);
            }
            var body = new
            {
                avgResult = Math.Round(avgRes / testResults.Count(), 1, MidpointRounding.ToPositiveInfinity),
                userIds = userIds,
                data = data
            };

            return Ok(body);
        }

        [HttpPost("Result/{id}")]
        public IActionResult GetResultByTestId(int id, [FromBody] CheckResultDTO[] checkResultDTOArr)
        {
            if (checkResultDTOArr == null)
            {
                return BadRequest("Object was null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }
            var questions = _testService.GetTestWithDetails(id).Questions;
            List<Answer> answers = new List<Answer>();
            foreach(var q in questions)
            {
                foreach(var answer in q.Answers)
                {
                    answers.Add(answer);
                }
            }
            
            List<CheckResultDTO> testAnswers = checkResultDTOArr.ToList();

            var questionsCount = testAnswers.Count;
            double eachQuestionPoints = 100.0 / questionsCount;
            double result = 0.0;

            foreach(var question in testAnswers)
            {
                double scoreForQuestion = 0;
                double correctIter = 0;
                double wrongIter = 0;
                int iter = 0;
                double rightAnswersCount = 0;

                foreach(var answer in question.Answers)
                {
                    if(answers.Where(a => a.Id == answer.AnswerId).FirstOrDefault().IsRightAnswer)
                    {
                        rightAnswersCount++;
                    }
                }

                foreach (var answer in question.Answers)
                {
                    iter++;
                    if(answer.Checked && answers.Where(a => a.Id == answer.AnswerId).FirstOrDefault().IsRightAnswer)
                    {
                        correctIter++;
                    }
                    else if(answer.Checked && !answers.Where(a => a.Id == answer.AnswerId).FirstOrDefault().IsRightAnswer)
                    {
                        wrongIter++;
                    }
                }
                if (correctIter == 0)
                {
                    scoreForQuestion = 0;
                }
                else 
                {
                    scoreForQuestion = ((correctIter / rightAnswersCount) - (wrongIter / rightAnswersCount)) * eachQuestionPoints;
                }
                if(scoreForQuestion <= 0)
                {
                    scoreForQuestion = 0;
                }

                result += scoreForQuestion;
            }
            if(result < 0)
            {
                result = 0;
            }
            else if(result > 100)
            {
                result = 100;  
            }

            return Ok(Math.Ceiling(result));
        }

        [HttpPut("{id}")]
        public IActionResult UpdateTestResult(int id, [FromBody] TestResultForCreationDTO testResultForCreationDTO)
        {
            if (testResultForCreationDTO == null)
            {
                return BadRequest("Object was null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var testResult = _testResultService.GetTestResultById(id);
            if(testResult == null)
            {
                return BadRequest($"No result for test {id}");
            }


            _testResultService.UpdateTestResult(testResultForCreationDTO);
            _unitOfwork.Save();

            return NoContent();
        }

        [HttpDelete]
        public IActionResult DeleteTestResult([FromBody] TestResultForCreationDTO testResultForCreationDTO)
        {
            if (testResultForCreationDTO == null)
            {
                return BadRequest("Object was null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var testResult = _testResultService.GetTestResultByUserAndTestId(_mapper.Map<TestResultDTO>(testResultForCreationDTO));
            if (testResult == null)
            {
                return BadRequest($"No result for test {testResultForCreationDTO.TestId}");
            }


            _testResultService.DeleteTestResult(testResultForCreationDTO);
            _unitOfwork.Save();

            return NoContent();
        }
    }
}
