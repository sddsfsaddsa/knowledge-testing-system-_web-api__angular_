﻿using DAL.Contracts;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class TestUserRepository : RepositoryBase<TestUser>, ITestUserRepository
    {
        public TestUserRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {

        }

        public void CreateTestUser(TestUser testUser)
        {
            testUser.Id = FindAll().Count() + 1;
            Create(testUser);
        }

        public void DeleteTestUser(TestUser testUser)
        {
            Delete(testUser);
        }

        public IEnumerable<TestUser> GetAllTestUsers()
        {
            return FindAll();
        }

        public IEnumerable<TestUser> GetAllTestUsersWithDetails()
        {
            return FindAll()
                .Include(tu => tu.Test);
        }

        public TestUser GetTestUserById(int id)
        {
            return FindByCondition(tu => tu.Id == id)
                .FirstOrDefault();
        }

        public TestUser GetTestUserWithDetails(int id)
        {
            return FindByCondition(tu => tu.Id == id)
                .Include(tu => tu.Test)
                .FirstOrDefault();
        }

        public void UpdateTestUser(TestUser testUser)
        {
            Update(testUser);
        }
    }
}
