﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class TestUser
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "User id is required")]
        public string UserId { get; set; }
        public User User { get; set; }

        [Required(ErrorMessage = "Test id is required")]
        public int TestId { get; set; }
        public Test Test { get; set; }
    }
}
