import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { UserForLogin } from '../interfaces/userForLogin';
import { UserForRegistration } from '../interfaces/UserForRegistration';
import { UserForUpdate } from '../interfaces/userForUpdate';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }

  formModel = this.formBuilder.group({
    UserName: ['', Validators.required],
    DateOfBirth: ['', Validators.required],
    Email: ['', [Validators.required, Validators.email]],
    Passwords: this.formBuilder.group({
      Password: ['', Validators.required],
      ConfirmPassword: ['', Validators.required]
    }, {validator: this.comparePasswords})
  
  })
  formLoginModel = this.formBuilder.group({
    Email: ['', [Validators.required, Validators.email]],
    Password: ['', Validators.required]
  })

  formUpdateModel = this.formBuilder.group({
    Name: ['', [Validators.required]],
    DateOfBirth: ['', Validators.required]
  })

  comparePasswords(fb: FormGroup){
    let confirmPswrdCtrl = fb.get('ConfirmPassword')
    if(confirmPswrdCtrl?.errors === null || 'passwordMismatch' in confirmPswrdCtrl!?.errors){
      if(fb.get('Password')?.value !== confirmPswrdCtrl?.value){
        confirmPswrdCtrl?.setErrors({passwordMismatch: true})
      }
      else{
        confirmPswrdCtrl?.setErrors(null)
      }
    }
  }

  registerUser(){
    var body : UserForRegistration = {
      Name: this.formModel.value.UserName,
      DateOfBirth: this.formModel.value.DateOfBirth,
      Email: this.formModel.value.Email,
      Password: this.formModel.value.Passwords.Password,
      ConfirmPassword: this.formModel.value.Passwords.Password,
    }

    return this.http.post(environment.baseApiUrl + 'accounts/register', body)      
    }

  loginUser(){
    var body : UserForLogin = {
      Email: this.formLoginModel.value.Email,
      Password: this.formLoginModel.value.Password
    }
    
    return this.http.post(environment.baseApiUrl + 'accounts/login', body)
  }

  getUsers(){
    return this.http.get(environment.baseApiUrl + 'users')
  }

  roleMatch(allowedRoles : string[]): boolean {
    var isMatch = false;
    var payLoad = JSON.parse(window.atob(localStorage.getItem('token')!.split('.')[1]));
    var userRole = payLoad.role;
    allowedRoles.forEach(element => {
      if (userRole == element) {
        isMatch = true;
        return false;
      }
      return isMatch
    });
    return isMatch;
  }

  updateUser(id: string){
    var body : UserForUpdate = {
      name: this.formUpdateModel.value.Name,
      dateOfBirth: this.formUpdateModel.value.DateOfBirth
    }
    
    return this.http.put(environment.baseApiUrl + 'users/' + id, body)
  }

  deleteUser(id: string){
    return this.http.delete(environment.baseApiUrl + 'users/' + id)
  }
}
