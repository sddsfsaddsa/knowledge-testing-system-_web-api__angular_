﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class TestResultService : ITestResultService
    {
        IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TestResultService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void AddTestResult(TestResultForCreationDTO testResult)
        {
            _unitOfWork.TestResult.AddTestResult(_mapper.Map<TestResult>(testResult));
        }

        public IEnumerable<TestResultDTO> GetAllTestResults()
        {
            return _unitOfWork.TestResult.GetAllTestResults()
                .Select(tr => _mapper.Map<TestResultDTO>(tr));
        }

        public TestResultDTO GetTestResultById(int id)
        {
            return _mapper.Map<TestResultDTO>(_unitOfWork.TestResult.GetTestResultById(id));
        }

        public TestResultDTO GetTestResultByUserAndTestId(TestResultDTO testResult)
        {
            return _mapper.Map<TestResultDTO>(_unitOfWork.TestResult
                .GetTestResultByUserAndTestId(_mapper.Map<TestResult>(testResult)));
        }

        public void UpdateTestResult(TestResultForCreationDTO testResult)
        {
            _unitOfWork.TestResult.UpdateTestResult(_mapper.Map<TestResult>(testResult));
        }

        public void DeleteTestResult(TestResultForCreationDTO testResult)
        {
            _unitOfWork.TestResult.DeleteTestResult(_mapper.Map<TestResult>(testResult));
        }
    }
}
