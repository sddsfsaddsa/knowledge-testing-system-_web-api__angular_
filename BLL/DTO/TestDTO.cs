﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace BLL.DTO
{
    public class TestDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [StringLength(100, ErrorMessage = "Title length can not be longer than 100 symbols")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [StringLength(500, ErrorMessage = "Description length can not be longer than 500 symbols")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Description is required")]
        public string AccessKey { get; set; }

        public IList<TestUser> TestUsers { get; set; }
        public List<Question> Questions { get; set; }
    }
}
