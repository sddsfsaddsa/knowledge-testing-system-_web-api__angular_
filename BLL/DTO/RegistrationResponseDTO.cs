﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class RegistrationResponseDTO
    {

        public IEnumerable<string> Errors { get; set; }
    }
}
