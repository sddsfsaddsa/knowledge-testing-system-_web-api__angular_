﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Contracts
{
    public interface ITestService
    {
        IEnumerable<TestDTO> GetAllTests();
        TestDTO GetTestById(int id);
        TestDTO GetTestWithDetails(int id);
        void AddTest(TestForCreationDTO testDto);
        void UpdateTest(TestDTO testDto);
        void DeleteTest(TestDTO testDto);
    }
}
