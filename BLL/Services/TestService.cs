﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class TestService : ITestService
    {
        IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<TestDTO> GetAllTests()
        {
            return _unitOfWork.Test.GetAllTests()
                .Select(test => _mapper.Map<TestDTO>(test));
        }

        public TestDTO GetTestById(int id)
        {
            return _mapper.Map<TestDTO>(_unitOfWork.Test.GetTestById(id));
        }

        public TestDTO GetTestWithDetails(int id)
        {
            return _mapper.Map<TestDTO>(_unitOfWork.Test.GetTestWithDetails(id));
        }

        public void AddTest(TestForCreationDTO testDto)
        {
            _unitOfWork.Test.CreateTest(_mapper.Map<Test>(testDto));
        }

        public void UpdateTest(TestDTO testDto)
        {
            _unitOfWork.Test.UpdateTest(_mapper.Map<Test>(testDto));
        }

        public void DeleteTest(TestDTO testDto)
        {
            _unitOfWork.Test.DeleteTest(_mapper.Map<Test>(testDto));
        }
    }
}
