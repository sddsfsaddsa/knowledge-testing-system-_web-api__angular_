﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AnswersController : ControllerBase
    {
        private readonly IAnswerService _answerService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public AnswersController(IAnswerService answerService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _answerService = answerService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAnswers()
        {
            var answers = _answerService.GetAllAnswers();
            return Ok(answers);
        }

        [HttpGet("{id}", Name = "AnswerById")]
        public IActionResult GetAnswerWithDetails(int id)
        {
            var answer = _answerService.GetAnswerWithDetails(id);
            if (answer == null)
            {
                return BadRequest("Answer was not found");
            }
            return Ok(answer);
        }

        [HttpPost]
        public IActionResult CreateAnswer([FromBody] AnswerForCreationDTO answerDto)
        {
            if (answerDto == null)
            {
                return BadRequest("Answer is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            _answerService.AddAnswer(answerDto);
            _unitOfWork.Save();
            var us = _answerService.GetAllAnswers().LastOrDefault();

            return CreatedAtRoute("AnswerById", new { id = us.Id }, answerDto);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateAnswer(int id, [FromBody] AnswerForCreationDTO answerDto)
        {
            if (answerDto == null)
            {
                return BadRequest("Answer is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var answer = _mapper.Map<Answer>(_answerService.GetAnswerById(id));
            if (answer == null)
            {
                return NotFound($"Answer with id: {id} hasn't been found");
            }

            _mapper.Map(answerDto, answer);

            _answerService.UpdateAnswer(_mapper.Map<AnswerDTO>(answer));
            _unitOfWork.Save();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteAnswer(int id)
        {
            var answer = _answerService.GetAnswerById(id);
            if (answer == null)
            {
                return BadRequest($"Answer with id: {id} was not found");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest($"Invalid model object");
            }

            _answerService.DeleteAnswer(answer);
            _unitOfWork.Save();

            return NoContent();
        }
    }
}
