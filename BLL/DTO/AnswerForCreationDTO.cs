﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.DTO
{
    public class AnswerForCreationDTO
    {
        [Required(ErrorMessage = "Answer's description is required")]
        [StringLength(200, ErrorMessage = "Answer's description length can not be longer than 200 symbols")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Answer's right variant is required")]
        public bool IsRightAnswer { get; set; }

        public int QuestionId { get; set; }
    }
}
