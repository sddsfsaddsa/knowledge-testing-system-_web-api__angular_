﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contracts
{
    public interface ITestResultService
    {
        public TestResultDTO GetTestResultById(int id);
        public IEnumerable<TestResultDTO> GetAllTestResults();
        public TestResultDTO GetTestResultByUserAndTestId(TestResultDTO testResult);
        void UpdateTestResult(TestResultForCreationDTO testResult);
        void AddTestResult(TestResultForCreationDTO testResult);
        void DeleteTestResult(TestResultForCreationDTO testResult);
    }
}
