﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QuestionsController : ControllerBase
    {
        private readonly IQuestionService _questionService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public QuestionsController(IQuestionService questionService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _questionService = questionService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetQuestions()
        {
            var questions = _questionService.GetAllQuestions();
            return Ok(questions);
        }

        [HttpGet("{id}", Name = "QuestionById")]
        public IActionResult GetQuestionWithDetails(int id)
        {
            var question = _questionService.GetQuestionWithDetails(id);
            if (question == null)
            {
                return BadRequest("Question was not found");
            }
            return Ok(question);
        }

        [HttpPost]
        public IActionResult CreateQuestion([FromBody] QuestionForCreationDTO questionDto)
        {
            if (questionDto == null)
            {
                return BadRequest("Question is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            _questionService.AddQuestion(questionDto);
            _unitOfWork.Save();
            var us = _questionService.GetAllQuestions().LastOrDefault();

            return CreatedAtRoute("QuestionById", new { id = us.Id }, questionDto);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateQuestion(int id, [FromBody] QuestionForCreationDTO questionDto)
        {
            if (questionDto == null)
            {
                return BadRequest("Question is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var question = _mapper.Map<Question>(_questionService.GetQuestionById(id));
            if (question == null)
            {
                return NotFound($"Question with id: {id} hasn't been found");
            }

            _mapper.Map(questionDto, question);

            _questionService.UpdateQuestion(_mapper.Map<QuestionDTO>(question));
            _unitOfWork.Save();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteQuestion(int id)
        {
            var question = _questionService.GetQuestionById(id);
            if (question == null)
            {
                return BadRequest($"Question with id: {id} was not found");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest($"Invalid model object");
            }

            _questionService.DeleteQuestion(question);
            _unitOfWork.Save();

            return NoContent();
        }
    }
}
