﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Contracts
{
    public interface ITestRepository : IRepositoryBase<Test>
    {
        public Test GetTestWithDetails(int id);
        public Test GetTestById(int id);
        public IEnumerable<Test> GetAllTests();
        void CreateTest(Test test);
        void UpdateTest(Test test);
        void DeleteTest(Test test);
    }
}
