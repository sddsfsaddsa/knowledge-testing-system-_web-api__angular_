import { AnswerInfo } from "./answerInfo";

export interface QuestionInfo{
  id: number
  description: string
  testId: number
  answers: AnswerInfo[]
}