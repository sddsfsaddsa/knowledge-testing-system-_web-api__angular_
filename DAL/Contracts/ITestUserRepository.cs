﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DAL.Contracts
{
    public interface ITestUserRepository : IRepositoryBase<TestUser>
    {
        public TestUser GetTestUserWithDetails(int id);
        public TestUser GetTestUserById(int id);
        public IEnumerable<TestUser> GetAllTestUsers();
        void CreateTestUser(TestUser testUser);
        void UpdateTestUser(TestUser testUser);
        void DeleteTestUser(TestUser testUser);
        IEnumerable<TestUser> GetAllTestUsersWithDetails();
    }
}
