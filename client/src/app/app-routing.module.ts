import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth/auth.guard';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { PasstestComponent } from './home/tests/passtest/passtest.component';
import { UserTestsComponent } from './admin-panel/user-tests/user-tests/user-tests.component';
import { QuestionsComponent } from './admin-panel/questions/questions/questions.component';
import { AnswersComponent } from './admin-panel/answers/answers/answers.component';
import { TestsComponent } from './user/actions/tests/tests.component';
import { TstComponent } from './admin-panel/tsts/tst/tst.component';
import { TeststatsComponent } from './admin-panel/tsts/teststats/teststats/teststats.component';
import { PagenotfoundComponent } from './notfound/pagenotfound/pagenotfound.component';

const routes: Routes = [
  { path: '', redirectTo: '/user/login', pathMatch: 'full' },
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'registration', component: RegistrationComponent },
      { path: 'login', component: LoginComponent }
    ]
  },
  { path: 'actions/tests', component: TestsComponent, canActivate: [AuthGuard], data: { permittedRoles: ['User', 'Admin'] } },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard], data: { permittedRoles: ['Admin', 'User'] } },
  { path: 'home/:id', component: PasstestComponent, canActivate: [AuthGuard], data: { permittedRoles: ['Admin', 'User'] } },
  { path: 'home/usertests/:id', component: UserTestsComponent, canActivate: [AuthGuard], data: { permittedRoles: ['Admin', 'User'] } },
  { path: 'forbidden', component: ForbiddenComponent },
  { path: 'adminpanel', component: AdminPanelComponent, pathMatch: 'full', canActivate: [AuthGuard], data: { permittedRoles: ['Admin'] } },
  { path: 'adminpanel/tests', component: TstComponent, pathMatch: 'full', canActivate: [AuthGuard], data: { permittedRoles: ['Admin'] } },
  { path: 'adminpanel/tests/teststats/:id', component: TeststatsComponent, pathMatch: 'full', canActivate: [AuthGuard], data: { permittedRoles: ['Admin'] } },
  { path: 'adminpanel/questions', component: QuestionsComponent, pathMatch: 'full', canActivate: [AuthGuard], data: { permittedRoles: ['Admin'] } },
  { path: 'adminpanel/answers', component: AnswersComponent, pathMatch: 'full', canActivate: [AuthGuard], data: { permittedRoles: ['Admin'] } },
  { path: '**', pathMatch: 'full', 
        component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }