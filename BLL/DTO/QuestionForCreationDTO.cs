﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.DTO
{
    public class QuestionForCreationDTO
    {
        [Required(ErrorMessage = "Question description is required")]
        [StringLength(200, ErrorMessage = "Question description length can not be longer than 200 symbols")]
        public string Description { get; set; }

        public int TestId { get; set; }
        public Test Test { get; set; }

        public List<Answer> Answers { get; set; }
    }
}
