﻿using AutoMapper;
using BLL.DTO;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Mappers
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(
                    dto => dto.TestusersTest,
                    opt => opt.MapFrom(src => src.Testusers.Select(t => t.Test))
                    ).ReverseMap();
          
            CreateMap<UserForCreationDTO, User>();
            CreateMap<User, UserForCreationDTO>();
            CreateMap<UserForRegistrationDTO, User>()
                .ForMember(u => u.UserName, opt => opt.MapFrom(x => x.Email))
                .ReverseMap(); ;
            
        }
    }
}
