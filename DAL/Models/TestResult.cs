﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class TestResult
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        public int TestId { get; set; }
        public Test Test { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public double Result { get; set; }
    }
}
