﻿using AutoMapper;
using BLL.DTO;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.MapperProfiles
{
    public class TestResultProfile : Profile
    {
        public TestResultProfile()
        {
            CreateMap<TestResult, TestResultForCreationDTO>()
                .ReverseMap();
            CreateMap<TestResult, TestResultDTO>()
                .ReverseMap();
            CreateMap<TestResultDTO, TestResultForCreationDTO>()
                .ReverseMap();
        }
    }
}
