﻿using DAL.Contracts;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class QuestionRepository : RepositoryBase<Question>, IQuestionRepository
    {
        public QuestionRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public void CreateQuestion(Question question)
        {
            Create(question);
        }

        public void DeleteQuestion(Question question)
        {
            Delete(question);
        }

        public IEnumerable<Question> GetAllQuestions()
        {
            return FindAll().ToList();
        }

        public Question GetQuestionById(int id)
        {
            return FindByCondition(q => q.Id == id)
              .FirstOrDefault();
        }

        public Question GetQuestionWithDetails(int id)
        {
            return FindByCondition(q => q.Id == id)
                .Include(q => q.Answers)
                .FirstOrDefault();
        }

        public void UpdateQuestion(Question question)
        {
            Update(question);
        }
    }
}
