﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class AnswerService : IAnswerService
    {
        IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public AnswerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void AddAnswer(AnswerForCreationDTO questionDto)
        {
            _unitOfWork.Answer.CreateAnswer(_mapper.Map<Answer>(questionDto));
        }

        public void DeleteAnswer(AnswerDTO questionDto)
        {
            _unitOfWork.Answer.DeleteAnswer(_mapper.Map<Answer>(questionDto));
        }

        public IEnumerable<AnswerDTO> GetAllAnswers()
        {
            return _unitOfWork.Answer.GetAllAnswers()
                .Select(q => _mapper.Map<AnswerDTO>(q));
        }

        public AnswerDTO GetAnswerById(int id)
        {
            return _mapper.Map<AnswerDTO>(_unitOfWork.Answer.GetAnswerById(id));
        }

        public AnswerDTO GetAnswerWithDetails(int id)
        {
            return _mapper.Map<AnswerDTO>(_unitOfWork.Answer.GetAnswerWithDetails(id));
        }

        public void UpdateAnswer(AnswerDTO questionDto)
        {
            _unitOfWork.Answer.UpdateAnswer(_mapper.Map<Answer>(questionDto));
        }
    }
}
