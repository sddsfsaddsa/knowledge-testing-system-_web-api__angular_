import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TestInfo } from '../interfaces/testInfo';
import { UserInfo } from '../interfaces/userInfo';
import { TestuserService } from '../shared/testuser.service';
import { UserService } from '../shared/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HideTest } from '../interfaces/hideTest';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.scss'
  ]
})



export class HomeComponent implements OnInit {

  hideTests: HideTest[] = []

  search = ''

  currentUserId: string = JSON.parse(window.atob(localStorage.getItem('token')!.split('.')[1])).UserID as string;
  users: UserInfo[] = []
  tests: TestInfo[] = []
  constructor(private router : Router,
     public service : UserService,
     public testUserService: TestuserService,
     private toastr: ToastrService) { }

  ngOnInit(): void {
    console.log(this.currentUserId)
    this.loadUserTests(this.currentUserId as string)

    
  }

  onLogout(){
    localStorage.removeItem('token')
    this.router.navigate(['/user/login'])
  }

  loadUserTests(id: string){
    this.testUserService.loadUserTests(id)
    .subscribe(
      res => {
        console.log(res)
        console.log(res)
        this.tests = res as TestInfo[]
      },
      err => {
        console.log(err)
        this.toastr.error(err.body, 'Error')
      }
    )
  }

  onSubmitAccessKey(){
    this.toastr.clear()
    let test :  TestInfo
    this.testUserService.findTestByKey()
    .subscribe(
      res => {
        test = res as TestInfo
        console.log("test id:", test.id)
        if(test.id > 0){
          if(this.tests.find(t => t.id === test.id)){
            this.toastr.error('You might already have this test or just no test with entered access key' ,'Error, new test not added')
          }
          else{
            this.testUserService.addTestUser(test.id, this.currentUserId)
            .subscribe(
              res => {
                this.toastr.success('New test added', 'Success')
                this.loadUserTests(this.currentUserId)
              },
              err => {
                console.log(err)
                this.toastr.error('You might already have this test or just no test with entered access key' ,'Error, new test mot added')
              }
            )
          }
       
      }
      else {
        this.toastr.error('No test with that access key', 'Invalid access key')
      }},
      err => {
        console.log(err)
        this.toastr.error(err.error, "Error: ")
      }
    )
  }

  passTest(id: number){
    
  }

  hideTest(id: number){
    var res = this.hideTests.find(test =>{
      console.log(`${test} + ${id} + ${test.testId === id}`)
      return test.testId === id
    })

    if(res?.passed){
      return true
    }
    return false
  }

  tryPassTest(){

  }


//   this.tests.forEach((test) =>{
//     var body =  {
//       testId: test.id,
//       passed: this.getTestResultByUserAndTestId(test.id)
//     }
//     this.hideTests.push(body)
// })
// console.log(this.hideTests)

}
