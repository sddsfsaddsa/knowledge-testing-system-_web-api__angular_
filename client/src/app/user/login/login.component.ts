import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public service: UserService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
    if(localStorage.getItem('token') !== null){
      this.router.navigateByUrl('/home')
    }
  }

  onSubmit(){
    this.toastr.clear()
    console.log('log')
    this.service.loginUser()
    .subscribe(
      (res: any ) => {
        localStorage.setItem('token', res.token)
        console.log("home")
        this.router.navigateByUrl('/home')
      },
      err => {
        if(err.status === 400){
          console.log(err)
          this.toastr.error('Incorrect email or password', 'Authentication failed', {timeOut: 0})
        }
        else{
          console.log('else:', err)
        }
      }
    )
  }

}
