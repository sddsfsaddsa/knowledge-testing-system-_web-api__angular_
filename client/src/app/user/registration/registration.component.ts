import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { responseErrors } from 'src/app/interfaces/responseErrors';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styles: [
  ]
})
export class RegistrationComponent implements OnInit {

  nowDate: string = new Date().toLocaleDateString()
  constructor(public service: UserService, private toastr: ToastrService) { }
  errors: String[] = []
  ngOnInit(): void {
    this.service.formModel.reset()
  }

  onSubmit(){
    this.toastr.clear()
     this.service.registerUser()
    .subscribe((res: any) =>{
      console.log(res)
      if(!res?.errors){
        this.service.formModel.reset()
        this.toastr.success('New user created!', 'Registration successful', {timeOut: 5000})
      }
      else{
        console.log("asdsd")
        let errorsStr = 'Errors: \n'
        let iter = 0
        res.errors.forEach((error: any) => {
          errorsStr += error + '\n'
          iter++
        })
        this.toastr.error(`\n${errorsStr}`, 'Registration failed', {timeOut: iter * 3000})
        }})
    }
  }
