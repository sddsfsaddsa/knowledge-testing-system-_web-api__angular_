﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Contracts
{
    public interface ITestResultRepository
    {
        public TestResult GetTestResultById(int id);
        public IEnumerable<TestResult> GetAllTestResults();
        public TestResult GetTestResultByUserAndTestId(TestResult testResult);
        void UpdateTestResult(TestResult testResult);
        void AddTestResult(TestResult testResult);
        void DeleteTestResult(TestResult testResult);
    }
}
