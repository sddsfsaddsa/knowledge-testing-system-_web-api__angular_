﻿using BLL.DTO;
using BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Contracts
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAllUsers();
        UserDTO GetUserById(Guid id);
        UserDTO GetUserWithDetails(Guid id);
        void AddUser(UserForCreationDTO userDto);
        void UpdateUser(UserDTO userDto);
        void DeleteUser(UserDTO userDto);        
    }
}
