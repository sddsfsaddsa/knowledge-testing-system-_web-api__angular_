﻿using DAL.Contracts;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
   public class AnswerRepository : RepositoryBase<Answer>, IAnswerRepository
    {
        public AnswerRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public void CreateAnswer(Answer answer)
        {
            Create(answer);
        }

        public void DeleteAnswer(Answer answer)
        {
            Delete(answer);
        }

        public IEnumerable<Answer> GetAllAnswers()
        {
            return FindAll().ToList();
        }

        public Answer GetAnswerById(int id)
        {
            return FindByCondition(a => a.Id == id)
                .FirstOrDefault();
        }

        public Answer GetAnswerWithDetails(int id)
        {
            return FindByCondition(a => a.Id == id)
                .FirstOrDefault();
        }

        public void UpdateAnswer(Answer answer)
        {
            Update(answer);
        }
    }
}
