﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestsController : ControllerBase
    {
        private readonly ITestService _testService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TestsController(ITestService testService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _testService = testService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetTests()
        {
            var tests = _testService.GetAllTests();
            return Ok(tests);
        }


        [HttpGet("{id}")]
        [Route("id/{id}")]
        public IActionResult GetTestById(int id)
        {
            var test = _testService.GetTestById(id);
            if (test == null)
            {
                return BadRequest("Test was not found");
            }
            return Ok(test);
        }

        [HttpGet("{testid}")]
        [Route("fulltest/{testid}")]
        public IActionResult GetFullTestById(int testid)
        {
            var test = _testService.GetTestWithDetails(testid);

            if (test == null)
            {
                return BadRequest("Test was not found");
            }
            return Ok(test);
        }


        [HttpGet("{key}")]
        [Route("key/{key}")]
        public IActionResult GetTestByKey(string key)
        {
            var test = _testService.GetAllTests().Where(t => t.AccessKey == key).FirstOrDefault();
            if (test == null)
            {
                return BadRequest("Test was not found");
            }
            return Ok(test);
        }

        




        [HttpPost]
        public IActionResult CreateTest([FromBody] TestForCreationDTO testDto)
        {
            if (testDto == null)
            {
                return BadRequest("Test is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            _testService.AddTest(testDto);
            _unitOfWork.Save();
            var us = _testService.GetAllTests().LastOrDefault();

            return Created("/tests", testDto);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateTest(int id, [FromBody] TestForCreationDTO testDto)
        {
            if (testDto == null)
            {
                return BadRequest("Test is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var test = _mapper.Map<Test>(_testService.GetTestById(id));
            if (test == null)
            {
                return NotFound($"Test with id: {id} hasn't been found");
            }

            _mapper.Map(testDto, test);

            _testService.UpdateTest(_mapper.Map<TestDTO>(test));
            _unitOfWork.Save();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTest(int id)
        {
            var test = _testService.GetTestById(id);
            if (test == null)
            {
                return BadRequest($"Test with id: {id} was not found");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest($"Invalid model object");
            }

            _testService.DeleteTest(test);
            _unitOfWork.Save();

            return NoContent();
        }
    }
}
