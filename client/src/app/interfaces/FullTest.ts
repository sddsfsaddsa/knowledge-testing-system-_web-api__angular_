import { QuestionInfo } from "./questionInfo";

export interface FullTest{
  questions: QuestionInfo[]
}