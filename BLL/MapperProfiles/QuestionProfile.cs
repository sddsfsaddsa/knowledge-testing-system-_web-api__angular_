﻿using AutoMapper;
using BLL.DTO;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.MapperProfiles
{
    public class QuestionProfile : Profile
    {
        public QuestionProfile()
        {
            CreateMap<Question, QuestionDTO>()
                .ForMember(
                    dto => dto.Answers,
                    opt => opt.MapFrom(src => src.Answers)
                    ).ReverseMap();
            CreateMap<QuestionForCreationDTO, Question>();
            CreateMap<Question, QuestionForCreationDTO>();
        }
    }
}
