export interface TestPassAnswer{
  answerId: number
  checked: boolean
}