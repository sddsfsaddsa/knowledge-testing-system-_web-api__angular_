import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TestInfo } from 'src/app/interfaces/testInfo';
import { UserTestResult } from 'src/app/interfaces/userTestResult';
import { UserTestInfo } from 'src/app/interfaces/userTestsInfo';
import { TestuserService } from 'src/app/shared/testuser.service';

@Component({
  selector: 'app-user-tests',
  templateUrl: './user-tests.component.html',
  styleUrls: ['./user-tests.component.scss']
})
export class UserTestsComponent implements OnInit {


  iter = 0
  currentTestId = 0
  currentUserId = ''

  userTestsInfo: UserTestInfo[] = []

  userTestsResults: UserTestResult[] = []

  testsOfUser: TestInfo[] = []
  tempTestsOfUser: TestInfo[] = []

  loadingData = false

  constructor(private route : ActivatedRoute, private testUserService: TestuserService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.toastr.clear()
    this.route.params.subscribe((params: Params) => {
      console.log(params)
      this.currentUserId = params.id
      this.loadUserTests()
    })

  }

  loadUserTests(){
    this.loadingData = true
    this.toastr.clear()
    this.testUserService.loadUserTests(this.currentUserId)
    .subscribe(
      res => {
        console.log(res)
        this.tempTestsOfUser = res as TestInfo[]
        this.loadingData = false
        console.log(this.tempTestsOfUser)
        this.testsOfUser = this.tempTestsOfUser
        this.getResults()
      },
      err => {
        this.toastr.error(err, 'Error')
      }
    )
    
  }

  dojob(){
    
  }

  getResults(){
    console.log(this.testsOfUser)
    this.testsOfUser.forEach((userTest) => {
      this.testUserService.getTestResultByuserAndTestId(this.currentUserId, userTest.id)
      .subscribe(
        res =>{
          console.log(`${this.currentUserId} ${userTest.id}: `, res)
          if((res as any).passed as boolean){
            console.log('true')
            console.log(`RessS ${this.iter}: `, this.userTestsResults)
            if(!this.userTestsResults.find((ut) => {
              return ut.testId == userTest.id
            })){
              this.userTestsResults.push({
                testId: userTest.id,
                result: (res as any).result as number
              })

              this.userTestsInfo.push({
                      id: userTest.id,
                      title: userTest.title,
                      description: userTest.description,
                      accessKey: userTest.accessKey,
                      result: (res as any).result as number
                    }
              )

              }
  
            }
          
            else{
              if(!this.userTestsResults.find((ut) => {
                return ut.testId == userTest.id
              })){
                this.userTestsResults.push({
                  testId: userTest.id,
                  result: "Not passed yet"
                })
  
                this.userTestsInfo.push({
                        id: userTest.id,
                        title: userTest.title,
                        description: userTest.description,
                        accessKey: userTest.accessKey,
                        result: "Not passed yet"
                      }
                )
  
                }
          }
        },
        err => {
          console.log('errorrr:', err)
        }
      )
    })
  }

}
