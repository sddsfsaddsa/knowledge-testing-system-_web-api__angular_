import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TestInfo } from '../interfaces/testInfo';
import { UserInfo } from '../interfaces/userInfo';
import { TestuserService } from '../shared/testuser.service';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {

  loadingData = false

  chosenUserId = ''
  previousChosenId = ''

  hiddenEdit = true
  hiddenDelete = true

  loading = false
  search = ''

  currentAdminId = ''

  allUsers: UserInfo[] = []
  users: UserInfo[] = []
  constructor(public service: UserService,
    private toastr: ToastrService,
    private testUserService: TestuserService,
    private router: Router) { }

  ngOnInit(): void {
    this.currentAdminId = JSON.parse(window.atob(localStorage.getItem('token')!.split('.')[1])).UserID as string;

    this.loadUsers()
  }

  loadUsers() {
    this.loadingData = true
    this.service.getUsers()
      .subscribe(res => {
        this.users = res as UserInfo[]
        this.users.splice(this.users.indexOf(
          this.users.find((u) => {
            return u.id === this.currentAdminId
          }) as UserInfo),
          1
        )
        this.loadingData = false
      })
    console.log(this.users)
  }

  showEditModal(id: string) {
    this.chosenUserId = id
    if (!this.previousChosenId) {
      this.previousChosenId = id
      this.chosenUserId = id
      this.hiddenEdit = !this.hiddenEdit
    }
    else if (this.previousChosenId) {
      if (this.previousChosenId === this.chosenUserId) {
        this.hiddenEdit = !this.hiddenEdit
      }
      else {
        this.previousChosenId = id
      }
    }

  }

  deleteUser(id: string) {
    this.toastr.clear()
    this.service.deleteUser(id)
      .subscribe(res => {
        console.log(res)
        this.toastr.success(`User ${id} was deleted`, 'Success')
      },
        err => {
          console.log(err)
          this.toastr.error(err, 'Error')
        })
  }

  onInput() {

  }

  onUpdateSubmit(id: string) {
    this.toastr.clear()
    this.service.updateUser(id)
      .subscribe(res => {
        console.log(res)

        console.log("users:", this.users)
        this.toastr.success(`User ${id} was updated`, 'Success')
      },
        err => {
          this.toastr.error(err, 'Error')
        })

        setTimeout(() => {
          this.loadUsers()
        }, 2000)
  }

  /*usertests*/

  testsOfUser: TestInfo[] = []
  temporaryTestsOfUser: TestInfo[] = []
  chosenUserForTestsId = ''

  loadUserTests() {

  }

  getTestResult(id: number) {

  }

  onLogout() {
    localStorage.removeItem('token')
    this.router.navigate(['/user/login'])
  }
}
