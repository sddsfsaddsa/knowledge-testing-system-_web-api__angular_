﻿using AutoMapper;
using BLL.DTO;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.MapperProfiles
{
    public class AnswerProfile : Profile
    {
        public AnswerProfile()
        {
            CreateMap<Answer, AnswerDTO>();
            CreateMap<AnswerDTO, Answer>();
            CreateMap<AnswerForCreationDTO, Answer>();
            CreateMap<Answer, AnswerForCreationDTO>();
        }
    }
}
