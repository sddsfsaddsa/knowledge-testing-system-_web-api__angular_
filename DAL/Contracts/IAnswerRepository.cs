﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Contracts
{
    public interface IAnswerRepository : IRepositoryBase<Answer>
    {
        public Answer GetAnswerWithDetails(int id);
        public Answer GetAnswerById(int id);
        public IEnumerable<Answer> GetAllAnswers();
        void CreateAnswer(Answer answer);
        void UpdateAnswer(Answer answer);
        void DeleteAnswer(Answer answer);
    }
}
