import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserService } from './shared/user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { FilterPipe } from './pipes/filter.pipe';
import { ModalComponent } from './modal/modal/modal.component';
import { TestuserService } from './shared/testuser.service';
import { TestsComponent } from './user/actions/tests/tests.component';
import { PasstestComponent } from './home/tests/passtest/passtest.component';
import { UserTestsComponent } from './admin-panel/user-tests/user-tests/user-tests.component';
import { QuestionsComponent } from './admin-panel/questions/questions/questions.component';
import { AnswersComponent } from './admin-panel/answers/answers/answers.component';
import { TstComponent } from './admin-panel/tsts/tst/tst.component';
import { ChartModule } from '@syncfusion/ej2-angular-charts';
import { CategoryService, ColumnSeriesService } from '@syncfusion/ej2-angular-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TeststatsComponent } from './admin-panel/tsts/teststats/teststats/teststats.component';
import { ProductBarChartsComponent } from './product-bar-charts/product-bar-charts.component';
import { PagenotfoundComponent } from './notfound/pagenotfound/pagenotfound.component';



@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    AdminPanelComponent,
    ForbiddenComponent,
    FilterPipe,
    ModalComponent,
    TestsComponent,
    PasstestComponent,
    UserTestsComponent,
    QuestionsComponent,
    AnswersComponent,
    TstComponent,
    TeststatsComponent,
    ProductBarChartsComponent,
    PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    ChartModule,
    NgxChartsModule
  ],
  providers: [UserService,
    CategoryService,
    ColumnSeriesService,
    TestuserService, {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
