﻿using DAL.Contracts;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private RepositoryContext _repoContext;
        private IUserRepository _user;
        private ITestRepository _test;
        private IQuestionRepository _question;
        private IAnswerRepository _answer;
        private ITestUserRepository _testUser;
        private ITestResultRepository _testResult;
        public IUserRepository User
        {
            get
            {
                if(_user == null)
                {
                    _user = new UserRepository(_repoContext); 
                }
                 return _user;
            }
        }
        public ITestRepository Test
        {
            get
            {
                if (_test == null)
                {
                    _test = new TestRepository(_repoContext);
                }
                return _test;
            }
         }
        public IQuestionRepository Question
        {
            get
            {
                if (_question == null)
                {
                    _question = new QuestionRepository(_repoContext);
                }
                return _question;
            }
        }
        public IAnswerRepository Answer
        {
            get
            {
                if (_answer == null)
                {
                    _answer = new AnswerRepository(_repoContext);
                }
                return _answer;
            }
        }

        public ITestUserRepository TestUser
        {
            get
            {
                if (_testUser == null)
                {
                    _testUser = new TestUserRepository(_repoContext);
                }
                return _testUser;
            }
        }

        public ITestResultRepository TestResult
        {
            get
            {
                if (_testResult == null)
                {
                    _testResult = new TestResultRepository(_repoContext);
                }
                return _testResult;
            }
        }

        public UnitOfWork(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public void Save()
        {
            _repoContext.SaveChanges();
        }

        public void Dispose()
        {
            _repoContext.Dispose();
        }
    }
}
