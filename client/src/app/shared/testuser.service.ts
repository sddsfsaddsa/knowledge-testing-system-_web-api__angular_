import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AccessKey } from '../interfaces/accessTest';
import { TestInfo } from '../interfaces/testInfo';
import { TestPassAnswer } from '../interfaces/testPassAnswers';
import { TestPassResponse } from '../interfaces/testPassResponse';


@Injectable({
  providedIn: 'root'
})
export class TestuserService {

  constructor(private formBuilder: FormBuilder,
     private http: HttpClient) { }

  

  formAccessTestModel = this.formBuilder.group(
   { 
     accessKey: ['', Validators.required]
  }
  )

  formUpdateTestModel = this.formBuilder.group(
    {
      title: ['', Validators.required],
      description: ['', Validators.required],
      accessKey: ['', Validators.required]
    }
  )

  formAddTestModel = this.formBuilder.group(
    {
      title: ['', Validators.required],
      description: ['', Validators.required],
      accessKey: ['', Validators.required]
    }
  )

  loadUserTests(id: string){
    return this.http.get(environment.baseApiUrl + 'testusers/' + id)
  }

  findTestByKey(key?: string){
    if(key?.trim()){
      return this.http.get(environment.baseApiUrl + 'tests/id/' + key)
    }
    else
    return this.http.get(environment.baseApiUrl + 'tests/key/' + this.formAccessTestModel.value.accessKey)
  }

  addTestUser(testId: number, userId: string){
    var body = {
      testId: testId,
      userId: userId
    }
    return this.http.post(environment.baseApiUrl + 'testusers', body)
  }

  loadFullTest(id: number){
    return this.http.get(environment.baseApiUrl + 'tests/fulltest/' + id)
  }

  getTestPassResponse(id: number, body: TestPassResponse[]){
    return this.http.post(environment.baseApiUrl + 'testresults/result/' + id, body)
  }

  addTestResult(userId: string, testId: number, testResult: number){
    console.log(testResult)
    var body = {
      userId: userId,
      testId: testId,
      result: testResult
    }
    return this.http.post(environment.baseApiUrl + 'testresults', body)
  }

  getTestResultByuserAndTestId(userId: string, testId: number){
    var body = {
      userId: userId,
      testId: testId
    }
    return this.http.post(environment.baseApiUrl + 'testresults/result', body)
  }

  getAllTests(){
    return this.http.get(environment.baseApiUrl + 'tests')
  }

  addTest(){
    var body = {
      title: this.formAddTestModel.value.title,
      description: this.formAddTestModel.value.description,
      accessKey: this.formAddTestModel.value.accessKey
    }
    return this.http.post(environment.baseApiUrl + 'tests', body)
  }

  updateTest(id: number){
    var body = {
      title: this.formUpdateTestModel.value.title,
      description: this.formUpdateTestModel.value.description,
      accessKey: this.formUpdateTestModel.value.accessKey
    }
    return this.http.put(environment.baseApiUrl + 'tests/' + id, body)
  }

  deleteTest(id: number){
    return this.http.delete(environment.baseApiUrl + 'tests/' + id)
  }

  getTestStats(id: number){
    return this.http.get(environment.baseApiUrl + 'testresults/' + id)
  }

}
