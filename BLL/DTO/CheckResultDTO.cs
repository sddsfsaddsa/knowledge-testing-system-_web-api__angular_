﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class CheckResultDTO
    {
        public string QuestionId { get; set; }
        public List<PassAnswerDTO> Answers { get; set; }
    }
}
