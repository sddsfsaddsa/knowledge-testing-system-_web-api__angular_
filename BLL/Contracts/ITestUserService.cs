﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Contracts
{
    public interface ITestUserService
    {
        IEnumerable<TestUserDTO> GetAllTestUsers();
        TestUserDTO GetTestUserById(int id);
        TestUserDTO GetTestUserWithDetails(int id);
        public IEnumerable<TestUserDTO> GetAllTestUsersWithDetails();
        void AddTestUser(TestUserForCreationDTO testUserDto);
        void UpdateTestUser(TestUserDTO testUserDto);
        void DeleteTestUser(TestUserDTO testUserDto);
    }
}
