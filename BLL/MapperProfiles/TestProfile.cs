﻿using AutoMapper;
using BLL.DTO;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.MapperProfiles
{
    public class TestProfile : Profile
    {
        public TestProfile()
        {
            CreateMap<Test, TestDTO>()
                .ForMember(
                    dto => dto.TestUsers,
                    opt => opt.MapFrom(src => src.TestUsers.Select(t => t.User))
                    ).ReverseMap();
            //CreateMap<TestDTO, Test>()
            //    .ForMember(
            //        dto => dto.TestUsers,
            //        opt => opt.MapFrom(src => src.TestUsers.Select(t => t.User))
            //        );
            CreateMap<TestForCreationDTO, Test>();
            CreateMap<Test, TestForCreationDTO>();
        }
    }
}
