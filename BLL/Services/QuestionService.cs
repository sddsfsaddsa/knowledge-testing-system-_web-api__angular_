﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class QuestionService : IQuestionService
    {
        IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public QuestionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void AddQuestion(QuestionForCreationDTO questionDto)
        {
            _unitOfWork.Question.CreateQuestion(_mapper.Map<Question>(questionDto));
        }

        public void DeleteQuestion(QuestionDTO questionDto)
        {
            _unitOfWork.Question.DeleteQuestion(_mapper.Map<Question>(questionDto));
        }

        public IEnumerable<QuestionDTO> GetAllQuestions()
        {
            return _unitOfWork.Question.GetAllQuestions()
                .Select(q => _mapper.Map<QuestionDTO>(q));
        }

        public QuestionDTO GetQuestionById(int id)
        {
            return _mapper.Map<QuestionDTO>(_unitOfWork.Question.GetQuestionById(id));
        }

        public QuestionDTO GetQuestionWithDetails(int id)
        {
            return _mapper.Map<QuestionDTO>(_unitOfWork.Question.GetQuestionWithDetails(id));
        }

        public void UpdateQuestion(QuestionDTO questionDto)
        {
            _unitOfWork.Question.UpdateQuestion(_mapper.Map<Question>(questionDto));
        }
    }
}
