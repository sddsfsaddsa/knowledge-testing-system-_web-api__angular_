﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace DAL.Models
{
    public class  User : IdentityUser
    {
        [Required(ErrorMessage = "Name is required")]
        [StringLength(60, ErrorMessage = "Name length can not be longer than 60 symbols")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Date of birth is required")]
        public DateTime DateOfBirth{ get; set; }
        public IList<TestUser> Testusers { get; set; }
        public List<TestResult> TestResults { get; set; }
    }
}
