export interface AnswerInfo{
  id: number
  description: string
  isRightanswer: boolean
  questionId: number
}