import { AfterContentInit, AfterViewChecked, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FullTest } from 'src/app/interfaces/FullTest';
import { TestInfo } from 'src/app/interfaces/testInfo';
import { TestuserService } from 'src/app/shared/testuser.service';
import { QuestionInfo } from 'src/app/interfaces/questionInfo';
import { AnswerInfo } from 'src/app/interfaces/answerInfo';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TestPassResponse } from 'src/app/interfaces/testPassResponse'
import { TestPassAnswer } from 'src/app/interfaces/testPassAnswers';
import { min } from 'rxjs/operators';

@Component({
  selector: 'app-passtest',
  templateUrl: './passtest.component.html',
  styleUrls: ['./passtest.component.scss']
})
export class PasstestComponent implements OnInit, AfterContentInit {

  timeLeft = 0

  timeLeftFormat = ""

  loadingData = false

  tryPassTestMsg = 'Pass the test'

  str: boolean = false

  myForm!: FormGroup;

  testPassResponseArr: TestPassResponse[] = []
  testPassResponse!: TestPassResponse

  answersResponseArr: TestPassAnswer[] = []
  answersResponse!: TestPassAnswer

  testResult: number = 0
  isTestPassed: boolean = false

  interval: any

  questionInfo: QuestionInfo = {
    id: 0,
    description: '',
    testId: 0,
    answers: []
  }
  resObj = {}
  fullTest: FullTest = {
    questions: [this.questionInfo]
  }
  testHidden: boolean = true
  currentTestId: number = 0
  currentUserId: string = ''
  currentTest: TestInfo = {
    id: 0,
    title: '',
    description: '',
    accessKey: ''
  }
  constructor(private route: ActivatedRoute, private testUserService: TestuserService,
    private toastr: ToastrService,
    public fb: FormBuilder) {


  }



  ngAfterContentInit() {
    this.checkIfAlreadyPassed(this.currentTestId)
    console.log(this.str)
    sessionStorage.setItem("timeLeft", "30")

  }

  ngOnInit(): void {
    this.toastr.clear()
    this.route.params.subscribe((params: Params) => {
      console.log(params)
      this.currentTestId = params.id
      this.currentUserId = JSON.parse(window.atob(localStorage.getItem('token')!.split('.')[1])).UserID as string;
      console.log(this.currentUserId)
      this.testUserService.findTestByKey(this.currentTestId.toString())
        .subscribe(
          res => {
            this.currentTest = res as TestInfo
          },
          err => {
            this.toastr.error('Something went wrong', 'Oops')
          }
        )
    })

    this.loadFullTest()
  }

  passTest() {
    console.log(this.str)
    this.testHidden = false
    let button = document.querySelector('button')
    if (button) {
      button.disabled = true
    }
    this.isTestPassed = false
    this.loadFullTest()
    this.timerStart()
  }

  timerStart() {

    this.interval = setInterval(() => {
      this.timeLeft = +(sessionStorage.getItem("timeLeft") as string)
      let minutes = Math.floor(this.timeLeft / 60)
      let seconds = this.timeLeft - minutes * 60
      this.timeLeftFormat = `${minutes}:${seconds}`
      sessionStorage.setItem("timeLeft", (this.timeLeft - 1).toString())
      console.log(this.timeLeft)
      var timer = document.getElementById('timer')
      if (this.timeLeft <= 0) {
        this.onSubmitFullTest()
      }
      if (this.timeLeft <= 60) {
        if (timer) {
          timer.style.fontSize = "40px"
          timer.style.backgroundColor = "red"
          timer.style.color = "white"
        }
      }

    }, 1000)

  }


  loadFullTest() {
    this.testUserService.loadFullTest(this.currentTestId)
      .subscribe(
        res => {
          this.resObj = res
          this.fullTest = res as FullTest
          console.log(res)
          console.log(this.resObj)
          this.loadingData = false
        },
        err => {
          console.log(err)
        }
      )

    this.loadingData = false
  }

  checkIfAlreadyPassed(id: number) {
    let isTestResult = false
    this.testUserService.getTestResultByuserAndTestId(this.currentUserId, id)
      .subscribe(
        res => {
          console.log(`test ${id}:`, res)
          isTestResult = (res as any).passed as boolean
          if (isTestResult) {
            this.testResult = (res as any).result as number
            this.isTestPassed = true
            var timer = document.getElementById('timer')
            if (timer) {
              timer.style.display = "none"
            }
            this.tryPassTestMsg = 'You have already passed this test'
          }
          this.str = isTestResult
          let btn = (document.getElementById('button')) as any
          (btn as HTMLButtonElement).disabled = isTestResult
        },
        err => {
          console.log("eErrr:", err)
        }
      )
    console.log(isTestResult)
  }

  onSubmitFullTest() {


    var questions = document.querySelectorAll(".questions .question-form")
    // console.log(questions)




    var result = 0
    var numOfAnswers = []
    var eachAnswerMax = 0
    var questionCount = questions.length
    var currQuestionId = 0
    // console.log("Q count: ", questionCount)
    questions.forEach(question => {
      // length += question.querySelectorAll(".answer").length

      currQuestionId = +(question.querySelector(".question-form .q-id")?.textContent)!
      // console.log(currQuestionId)
      var answerInputs = question.querySelectorAll(".answers .answer .input-custom")
      console.log(answerInputs.length)
      answerInputs.forEach(input => {
        // console.log((input as HTMLFormElement).checked)
        // console.log((input as HTMLFormElement).id)
        this.answersResponse = {
          answerId: +(input as HTMLFormElement).id,
          checked: (input as HTMLFormElement).checked
        }
        this.answersResponseArr.push(this.answersResponse)

      });
      this.testPassResponse = {
        questionId: +currQuestionId!,
        answers: this.answersResponseArr
      }
      this.testPassResponseArr.push(this.testPassResponse)
      console.log("RESPONSE: ", this.testPassResponseArr)
      // for (const prop of Object.getOwnPropertyNames(this.testPassResponse)) {
      //   delete (this.testPassResponse as any)[prop];
      // }
      this.answersResponseArr = []
    });

    console.log("BAdaDDYYYY: ", this.testPassResponseArr)
    console.log("All tests count: ", length)
    console.log("First question: ", questions[0])
    console.log((questions[0].querySelectorAll(".answers .answer .form-check-input")))
    console.log((questions[0].querySelectorAll(".answers .answer .form-check-input").length))


    this.testUserService.getTestPassResponse(this.currentTestId, this.testPassResponseArr)
      .subscribe(
        ress => {
          console.log("AAA")
          console.log(ress)
          this.testResult = ress as number
          this.testUserService.addTestResult(this.currentUserId, this.currentTestId, ress as number)
            .subscribe(
              res => {
                console.log(res)
              },
              err => {
                console.log(err)
              }
            )
          this.testPassResponseArr = []
        },
        err => {
          console.log("BBB")
          console.log(err)
        }
      )
    this.testHidden = true
    this.isTestPassed = true
    clearInterval(this.interval)


  }



}
