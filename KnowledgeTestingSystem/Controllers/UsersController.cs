﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL;
using DAL.Contracts;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace KnowledgeTestingSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly RepositoryContext _context;
        private readonly UserManager<User> _userManager;
        public UsersController(IUserService userService, IUnitOfWork unitOfWork, IMapper mapper, RepositoryContext context, UserManager<User> userManager)
        {
            _userService = userService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _context = context;
            _userManager = userManager;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult GetUsers()
        {
            var users = _userService.GetAllUsers();
            return Ok(users);
        }

        [HttpGet("{id}", Name = "UserById")]
        [Authorize(Roles = "Admin")]
        public IActionResult GetUserWithDetails(Guid id)
        {
            var user = _userService.GetUserWithDetails(id);
            if (user == null) {
                return BadRequest("User was not found");
            }
            return Ok(user);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult CreateUser([FromBody] UserForCreationDTO userDto)
        {
            if (userDto == null)
            {
                return BadRequest("User is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            _userService.AddUser(userDto);
            _unitOfWork.Save();
            var us = _userService.GetAllUsers().LastOrDefault();

            return CreatedAtRoute("UserById", new { id = us.Id }, userDto);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateUserAsync(Guid id, [FromBody] UserForCreationDTO userDto)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (userDto == null)
            {
                return BadRequest("User is null");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }
            if(user == null)
            {
                return NotFound($"User with id: {id} hasn't been found");
            }

            _mapper.Map(userDto, user);
            await _userManager.UpdateAsync(user);

           _unitOfWork.Save();

            return NoContent();
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteUserAsync(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if(user == null)
            {
                return BadRequest($"User with id: {id} was not found");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest($"Invalid model object");
            }

            await _userManager.DeleteAsync(user);
            _unitOfWork.Save();

            return NoContent();
        }
    }
}
