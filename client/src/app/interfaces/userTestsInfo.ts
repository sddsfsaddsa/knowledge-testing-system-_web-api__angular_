export interface UserTestInfo{
  id: number
  title: string
  description: string
  accessKey: string
  result: number | string
}