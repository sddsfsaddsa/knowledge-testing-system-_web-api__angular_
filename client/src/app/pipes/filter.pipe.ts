import { Pipe, PipeTransform } from '@angular/core';
import { UserInfo } from '../interfaces/userInfo';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform<Type>(items: Type[], search: string) {
    if(items == null){
      return 
    }
    
    if(!search.trim()){
      return items
    }
    console.log(items)

    const searchArr = search.toLowerCase().split(',')

    return items.filter(item => {
      const properties = Object.getOwnPropertyNames(item)
      let match = false
      properties.forEach(prop =>{
        searchArr.forEach(str => {
          if((item as any)[prop] == null){

          }
          else if((item as any)[prop].toString().toLowerCase().includes(str.trim().toLowerCase())){
            match = true
          }
        })
      }
      )
      return match
      

    })
  }

}
