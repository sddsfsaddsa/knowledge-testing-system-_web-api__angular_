﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.DTO
{
    public class TestUserForCreationDTO
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public int TestId { get; set; }
    }
}
