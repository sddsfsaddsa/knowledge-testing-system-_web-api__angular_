import { TestPassAnswer } from "./testPassAnswers";

export interface TestPassResponse {
  questionId: number
  answers: TestPassAnswer[]
}