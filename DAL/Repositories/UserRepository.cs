﻿using DAL.Contracts;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public void CreateUser(User user)
        {
            var u = user;
            Create(user);
        }

        public void DeleteUser(User user)
        {
            Delete(user);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return FindAll();
        }

        public User GetUserById(Guid userId)
        {
          return FindByCondition(u => u.Id == userId.ToString())
               .FirstOrDefault();
        }

        public User GetUserWithDetails(Guid userId)
        {
            var u = FindByCondition(u => u.Id == userId.ToString())
                .Include(u => u.Testusers)
                .FirstOrDefault();
            return FindByCondition(u => u.Id == userId.ToString())
                .Include(u => u.Testusers)
                    .ThenInclude(tu => tu.Test)
               .FirstOrDefault();
        }

        public void UpdateUser(User user)
        {
            Update(user);
        }
    }
}
