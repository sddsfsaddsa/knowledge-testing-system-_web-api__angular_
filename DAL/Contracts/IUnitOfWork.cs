﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Contracts
{
    public interface IUnitOfWork
    {
        IUserRepository User { get; }
        ITestRepository Test{ get; }
        IQuestionRepository Question{ get; }
        IAnswerRepository Answer{ get; }
        ITestUserRepository TestUser{ get; }
        ITestResultRepository TestResult{ get; }
        void Save();
    }
}
