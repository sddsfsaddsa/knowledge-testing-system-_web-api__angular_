export interface UserTestResult {
  testId: number
  result: number | string
}