export interface TestStatsForChart{
  avgResult: number
  userIds: string[]
  data: number[]
}