﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class TestResultDTO
    {
        public int Id { get; set; }

        public int TestId { get; set; }
        
        public string UserId { get; set; }

        public double Result { get; set; }

    }
}
