﻿using DAL.Contracts;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class TestResultRepository : RepositoryBase<TestResult>, ITestResultRepository
    {
        public TestResultRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
        public void AddTestResult(TestResult testResult)
        {
            Create(testResult);
        }

        public void DeleteTestResult(TestResult testResult)
        {
            Delete(testResult);
        }

        public IEnumerable<TestResult> GetAllTestResults()
        {
            return FindAll();
        }

        public TestResult GetTestResultById(int id)
        {
            return FindByCondition(tr => tr.Id == id)
                .FirstOrDefault();
        }

        public TestResult GetTestResultByUserAndTestId(TestResult testResult)
        {
            return FindAll()
                .FirstOrDefault(tr => tr.TestId == testResult.TestId && tr.UserId == testResult.UserId);
        }

        public void UpdateTestResult(TestResult testResult)
        {
            Update(testResult);
        }
    }
}
