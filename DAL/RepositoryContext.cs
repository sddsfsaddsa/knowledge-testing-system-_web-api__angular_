﻿using DAL.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class RepositoryContext : IdentityDbContext<User>
    {
        public override DbSet<User> Users { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<TestUser> TestUsers { get; set; }
        public DbSet<TestResult> TestResult { get; set; }

        public RepositoryContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //The value of 'TestUser.TestId' is unknown when attempting to save changes.
            //This is because the property is also part of a foreign key for which the principal entity in the relationship is not known

            modelBuilder.Entity<TestResult>().HasKey(tr => new { tr.UserId, tr.TestId });


            modelBuilder.Entity<TestResult>()
                .HasOne<User>(sc => sc.User)
                .WithMany(s => s.TestResults)
                .HasForeignKey(sc => sc.UserId);

            modelBuilder.Entity<TestResult>()
                .HasOne<Test>(sc => sc.Test)
                .WithMany(s => s.TestResults)
                .HasForeignKey(sc => sc.TestId);


            //modelBuilder.Entity<TestResult>()
            //  .HasOne(a => a.User)
            //  .WithOne(b => b.TestResult)
            //  .HasForeignKey(tr => tr.UserId).IsRequired();

            //modelBuilder.Entity<TestResult>()
            //.HasOne(a => a.Test)
            //.WithOne(b => b.TestResult)
            //.HasForeignKey(a => a.)
            

            modelBuilder.Entity<TestUser>().HasKey(tu => new { tu.UserId, tu.TestId });

            modelBuilder.Entity<TestUser>()
                .HasOne<User>(sc => sc.User)
                .WithMany(s => s.Testusers)
                .HasForeignKey(sc => sc.UserId);


            modelBuilder.Entity<TestUser>()
                .HasOne<Test>(sc => sc.Test)
                .WithMany(s => s.TestUsers)
                .HasForeignKey(sc => sc.TestId);

          

            var guid1 = Guid.NewGuid().ToString();
            var guid2 = Guid.NewGuid().ToString();
            var guid3 = Guid.NewGuid().ToString();
            var guid4 = Guid.NewGuid().ToString();

            modelBuilder.Entity<TestUser>().HasData(
               new { Id = 1, TestId = 1, UserId = guid1 },
               new { Id = 2, TestId = 1, UserId = guid2 },
               new { Id = 3, TestId = 2, UserId = guid3 },
               new { Id = 4, TestId = 2, UserId = guid4 }
               );
            modelBuilder.Entity<User>().HasData(
               new User[]
               {
                new User {Id = guid1, Name = "Aaa", DateOfBirth = new DateTime(1111, 1, 1) },
                new User {Id = guid2, Name = "Bbb", DateOfBirth = new DateTime(1111, 1, 1) },
                new User {Id = guid3, Name = "Ccc", DateOfBirth = new DateTime(1111, 1, 1) },
                new User {Id = guid4, Name = "Ddd", DateOfBirth = new DateTime(1111, 1, 1) }
               });

            modelBuilder.Entity<Test>().HasData(
                new Test[]
                {
                    new Test{
                        Id = 1,
                        AccessKey = "1a",
                        Description = "first",
                        Title = "Test1"
                    },
                    new Test{
                        Id = 2,
                        AccessKey = "2b",
                        Description = "second",
                        Title = "Test2"
                    },
                });

           

            modelBuilder.Entity<Question>().HasData(
               new Question[]
               {
                    new Question{Id = 1, Description = "first", TestId = 1},
                    new Question{Id = 2, Description = "first", TestId = 2},
               });

           modelBuilder.Entity<Answer>().HasData(
                new Answer[]
                {
                    new Answer{Id = 1, Description = "Ok1", IsRightAnswer = true, QuestionId = 1},
                    new Answer{Id = 2, Description = "Wrong1", IsRightAnswer = false, QuestionId = 1},
                    new Answer{Id = 3, Description = "Ok2", IsRightAnswer = true, QuestionId = 2},
                    new Answer{Id = 4, Description = "Wrong2", IsRightAnswer = false,  QuestionId = 2},
                    new Answer{ Id = 5,Description = "Ok3", IsRightAnswer = true, QuestionId = 1},
                    new Answer{ Id = 6,Description = "Wrong3", IsRightAnswer = false, QuestionId = 1},
                    new Answer{ Id = 7,Description = "Ok4", IsRightAnswer = true,  QuestionId = 2},
                    new Answer{ Id = 8,Description = "Wrong4", IsRightAnswer = false,  QuestionId = 2}
                });
           

        }
    }
}
