import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TestStatsData } from 'src/app/interfaces/testStatsData';
import { TestStatsForChart } from 'src/app/interfaces/testStatsForChart';
import { TestuserService } from 'src/app/shared/testuser.service';


@Component({
  selector: 'app-teststats',
  templateUrl: './teststats.component.html',
  styleUrls: ['./teststats.component.scss']
})
export class TeststatsComponent implements OnInit {

   view: [number,number] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'User ids';
  showYAxisLabel = true;
  yAxisLabel = 'Test result';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  testStatsForChart!: TestStatsForChart

  dataForChart: any[]  = []

  data = [
    {
      "name": "15188827-4a9f-4912-b5a9-facbe2466e48",
      "value": 501,
    },
    {
      "name": "w",
      "value": 30,
    },
    {
      "name": "e",
      "value": 200,
    }
  ]

  currentTestId = 0

  constructor(private route: ActivatedRoute, private toastr: ToastrService,
    private service: TestuserService) {

     }

  ngOnInit(): void {
    this.toastr.clear()
    this.route.params.subscribe((params: Params) => {
      console.log(params)
      this.currentTestId = params.id
      this.getTestStats()
    })
  }

  getTestStats(id?: number) {
    if(id) this.currentTestId = id
    this.toastr.clear()
    this.service.getTestStats(this.currentTestId)
      .subscribe(
        res => {
          this.testStatsForChart = res as TestStatsForChart
          var temp = {
            userIds: this.testStatsForChart.userIds,
            results: this.testStatsForChart.data
          }
          for (let step = 0; step < temp.userIds.length; step++) {
            this.dataForChart.push({
              name: temp.userIds[step],
              value: temp.results[step]
            })
          }
          this.dataForChart = [...this.dataForChart]
          console.log(this.dataForChart)
          console.log(this.testStatsForChart)
        },
        err => {
          this.toastr.error(err.error, "Error: ");
        }
      )
  }

}
