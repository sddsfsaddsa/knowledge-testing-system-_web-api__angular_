import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TestInfo } from 'src/app/interfaces/testInfo';
import { TestStatsData } from 'src/app/interfaces/testStatsData';
import { TestStatsForChart } from 'src/app/interfaces/testStatsForChart';
import { TestuserService } from 'src/app/shared/testuser.service';

@Component({
  selector: 'app-tst',
  templateUrl: './tst.component.html',
  styleUrls: ['./tst.component.scss']
})
export class TstComponent implements OnInit {

  testStatsForChart!: TestStatsForChart

  dataForChart: TestStatsData[] = []

  loadingData = false
  hiddenEdit = true
  hiddenAdd = true

  tests: TestInfo[] = []

  search = ''

  chosenTestId = 0
  previousChosenId = 0

  constructor(private router: Router,
    public service: TestuserService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.loadTests()
  }

  loadTests() {
    this.loadingData = true
    this.service.getAllTests()
      .subscribe(
        res => {
          this.tests = res as TestInfo[]
          this.loadingData = false
        },
        err => {
          console.log(err)
        }
      )
  }

  showEditModal(id: number) {
    this.chosenTestId = id
    if (this.previousChosenId === 0) {
      this.previousChosenId = id
      this.chosenTestId = id
      this.hiddenEdit = !this.hiddenEdit
    }
    else if (this.previousChosenId !== 0) {
      if (this.previousChosenId === this.chosenTestId) {
        this.hiddenEdit = !this.hiddenEdit
      }
      else {
        this.previousChosenId = id
      }
    }
  }

  deleteTest(id: number) {
    this.toastr.clear()
    this.service.deleteTest(id)
      .subscribe(
        res => {
          this.tests.splice(this.tests.indexOf(
            this.tests.find((t) => {
              return t.id === id
            }) as TestInfo),
            1
          )
          this.toastr.success(`Test ${id} was deleted`, 'Successfully deleted')
        },
        err => {
          this.toastr.error(err.error, 'Error: ')
        }
      )
  }

  onUpdateSubmit(id: number) {
    this.toastr.clear()
    this.service.updateTest(id)
      .subscribe(
        res => {
          this.toastr.success(`Test ${id} was updated`, 'Successfully updated')
        },
        err => {
          this.toastr.error(err.error, 'Error: ')
        }
      )
  }

  onAddSubmit() {
    this.toastr.clear()
    this.service.addTest()
      .subscribe(
        res => {
          this.tests.push(res as TestInfo)
          this.toastr.success(`Test was created`, 'Successfully created')
        },
        err => {
          this.toastr.error(err.error, 'Error: ')
        }
      )
  }

  addTest() {
    this.hiddenAdd = !this.hiddenAdd
  }

  onLogout() {
    localStorage.removeItem('token')
    this.router.navigate(['/user/login'])
  }

}
