﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public  class TestResultForCreationDTO
    {
        public int TestId { get; set; }

        public string UserId { get; set; }
        
        public double Result { get; set; }
    }
}
