﻿using AutoMapper;
using BLL.Contracts;
using BLL.DTO;
using DAL.Contracts;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class TestUserService : ITestUserService
    {
        IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TestUserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void AddTestUser(TestUserForCreationDTO testUserDto)
        {
            var a = _mapper.Map<TestUser>(testUserDto);
            _unitOfWork.TestUser.CreateTestUser(_mapper.Map<TestUser>(testUserDto));
        }

        public void DeleteTestUser(TestUserDTO testUserDto)
        {
            _unitOfWork.TestUser.DeleteTestUser(_mapper.Map<TestUser>(testUserDto));
        }

        public IEnumerable<TestUserDTO> GetAllTestUsers()
        {
            return _unitOfWork.TestUser.GetAllTestUsers()
                .Select(tu => _mapper.Map<TestUserDTO>(tu));
        }
        public IEnumerable<TestUserDTO> GetAllTestUsersWithDetails()
        {
            var u = _unitOfWork.TestUser.GetAllTestUsersWithDetails()
                .Select(tu => _mapper.Map<TestUserDTO>(tu));
            return _unitOfWork.TestUser.GetAllTestUsersWithDetails()
                .Select(tu => _mapper.Map<TestUserDTO>(tu));
        }

        public TestUserDTO GetTestUserById(int id)
        {
            return _mapper.Map<TestUserDTO>(_unitOfWork.TestUser.GetTestUserById(id));
        }

        public TestUserDTO GetTestUserWithDetails(int id)
        {
            return _mapper.Map<TestUserDTO>(_unitOfWork.TestUser.GetTestUserWithDetails(id));
        }

        public void UpdateTestUser(TestUserDTO testUserDto)
        {
            _unitOfWork.TestUser.UpdateTestUser(_mapper.Map<TestUser>(testUserDto));
        }
    }
}
