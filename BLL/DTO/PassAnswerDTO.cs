﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class PassAnswerDTO
    {
        public int AnswerId { get; set; }
        public bool Checked { get; set; }
    }
}
