﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Contracts
{
    public interface IAnswerService
    {
        IEnumerable<AnswerDTO> GetAllAnswers();
        AnswerDTO GetAnswerById(int id);
        AnswerDTO GetAnswerWithDetails(int id);
        void AddAnswer(AnswerForCreationDTO answerDto);
        void UpdateAnswer(AnswerDTO answerDto);
        void DeleteAnswer(AnswerDTO answerDto);
    }
}
