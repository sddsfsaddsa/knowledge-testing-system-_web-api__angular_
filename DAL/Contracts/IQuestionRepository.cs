﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Contracts
{
    public interface IQuestionRepository : IRepositoryBase<Question>
    {
        public Question GetQuestionWithDetails(int id);
        public Question GetQuestionById(int id);
        public IEnumerable<Question> GetAllQuestions();
        void CreateQuestion(Question question);
        void UpdateQuestion(Question question);
        void DeleteQuestion(Question question);
    }
}
