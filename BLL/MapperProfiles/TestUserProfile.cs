﻿using AutoMapper;
using BLL.DTO;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.MapperProfiles
{
    public class TestUserProfile : Profile
    {
        public TestUserProfile()
        {
            CreateMap<TestUser, TestUserDTO>()
                .ReverseMap();
            CreateMap<TestUser, TestUserForCreationDTO>()
                .ReverseMap();
        }
    }
}
